import time

#contadores
contador=1
intento = 3 
i=0
id=0
costo_huevo=0

#contraseña ADM
pass_adm= '1234'

#LISTAS
gallinas_disponibles = ['GALLINA','PATO','CODORNIZ','AVESTRUZ']
precio_huevos = [50,150,50,800]
precio_actualizados = [0,0,0,0]

#ALMACENADO
datos_despacho = {}

while contador == 1  : 
    pass_word = input(" Usuario ADM \n Ingrese su contraseña: ")
    if pass_word == pass_adm :
        opcion= 'a'
        
        while opcion != 'e':
            
            print("--------------  \n MENU PRINCIPAL \n --------------")
            print("a .- Asignar precios de Huevos.")
            print("b .- Crear de Despachos.")
            print("c .- Listar Huevos.")
            print("d .- Listar Despachos")
            print("e .- Salir del programa.")
            opcion = input("Seleccione una opcion: ")
            if opcion == 'a':
                print("LOS HUEVOS TIENEN QUE TENER UN PRECIO MAYOR A 50,150,50,800 \n")
                #gallina
                precio_gallina = int( input("Ingrese precio Gallina : "))
                if precio_huevos[0] <= precio_gallina :
                    precio_actualizados[0] = precio_gallina
                else:
                    print("dato invalido")
                #pato
                precio_pato = int( input("Ingrese precio Pato: "))
                if precio_huevos[1] <= precio_pato :
                    precio_actualizados[1] = precio_pato
                else:
                    print("dato invalido")
                #Codorniz
                precio_codorniz = int( input("Ingrese precio Codorniz : "))
                if precio_huevos[2] <= precio_codorniz :
                    precio_actualizados[2] = precio_codorniz
                else:
                    print("dato invalido")
                #avestruz
                precio_avestruz = int( input("Ingrese precio Avestruz : "))
                if precio_huevos[3] <= precio_avestruz :
                    precio_actualizados[3] = precio_avestruz 
                else:
                    print("dato invalido")
                #INGRESAR DESPACHO
            if opcion == 'b':
                #datos ingresados
                rut_cliente = input("Ingrese rut del cliente\n(considere guion,ultimo digito y las respuestas con MAYUSCULAS) \n ")
                nombre_cliente = input("Ingrese su nombre\n")
                tipo_huevo = input("Ingrese el tipo de huevo (GALLINA,PATO,CODORNIZ,AVESTRUZ)\n")
                convenio = input("tiene algun convenio? SI/NO \n")
                direc_despacho = input("Ingrese su direccion: \n")
                fecha_despacho = input("Ingresar fecha del despacho (ejemplo: 25-06-2022)\n")
                cant_huevos = int(input("Ingrese cantidad de huevos minumo 50 y maximo 10.000: (ejemplo: 380)\n"))

                #datos a calcular
                while i < 4 :
                    if(tipo_huevo == gallinas_disponibles[i]):
                        costo_huevo = cant_huevos * precio_actualizados[i]
                        break
                    else:
                        i = i+1
                print(costo_huevo)
                

                #reglas a considerar
                if(rut_cliente == '' or nombre_cliente == '' or tipo_huevo == '' or convenio == '' or fecha_despacho == '' or cant_huevos == ''):
                    print("Algun dato incompleto.... Vuelva a intentarlo")
                    time.sleep(3)
                    break
                elif(cant_huevos < 50 or cant_huevos > 10000):
                    print("Cantidad de huevos no esta en el rango.... Vuelva a intentarlo")
                    time.sleep(3)
                    break
                #convenio
                if(convenio == 'SI'):
                    descuento = costo_huevo * 0.1
                    costo_despacho_total = costo_huevo - descuento
                else:
                    costo_despacho_total = costo_huevo
                
                #REGISTRO DESPACHOS
                id= id + 1
                lista = ['despacho']
                lista.append(id)
                lista.append(rut_cliente)
                lista.append(nombre_cliente)
                lista.append(tipo_huevo)
                lista.append(convenio)
                lista.append(direc_despacho)
                lista.append(fecha_despacho)
                lista.append(cant_huevos)
                lista.append(costo_despacho_total)
                datos_despacho[id] = lista
                print(lista)
                (print("Registro almacenado con exito...!"))

            #LISTR PRECIOS ACTUALIZADOS
            if opcion == 'c':
                print("\n \n")
                print("HUEVOS DISPONIBLES:  ")
                print("Huevo Gallina con el precio: ", precio_actualizados[0])
                print("Huevo Pato con el precio: ", precio_actualizados[1])
                print("Huevo Codorniz con el precio: ", precio_actualizados[2])
                print("Huevo Avestruz con el precio: ", precio_actualizados[3])

            #LISTAR DESPACHOS
            if opcion == 'd':
                print(datos_despacho) 

            #FINALIZAR
            if opcion == 'e':
                contador = 0                  

    else :
        #tiene 3 intentos para ingresar bien la contraseña sino el programa se cierra
        intento = intento -1 
        print("Vuelve a intentarlo tienes ", intento, " intentos")
        if(intento == 0):
            contador = 0
            print("--------------")

    
    print("programa finalizado")
